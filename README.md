[![pipeline status](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/stack-test/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/stack-test/-/commits/master)

# Online SW Test Stack
Repository to test integration between the Shep and HERD aspects of the CMS Ph2 Online Software project. Included within this repository is a [docker-compose.yml](docker-compose.yml) which will allow the user to create a standalone emulation of the shepHERD stack. Included within this stack is:
- A set of [HERD-dummy](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy) instances, which provide a representative subset of the functionality provided by endpoints in the cluster. These instances run as standalone containers.
- A single [shep](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep) instance, which can control the HERD-dummy instances.
- A simplified broker, which routes commands sent from the shep container out to the HERD-dummy containers and also registers the HERD instances to a database. 

<img src="/docs/block-diagram.png"  width="600">

### Prerequisites
In order to run the stack, both `docker` and `docker-compose` must be installed.

### Running the Stack Locally
> Note: following these instructions creates the stack from pre-created images. If you intend to make changes to the codebase, please refer to the dev [section](#running-the-stack-locally-development) 
1. Dowload or copy the content of [docker-compose.yml](docker-compose.yml) to your machine.
2. Navigate to the directory containing the `docker-compose.yml` file.
3. Run `docker-compose pull` to pull the images from the repository.
4. Run `docker-compose up` to stack the containers.
5. Open a web browser and navigate to `http://localhost` to access the shep web interface.

### Running the Stack Locally (development)
1. Clone the repository to your local machine and navigate to the top level of the repository.
2. Run `docker-compose up --build` to build then run the containers in the stack.
3. Open a web browser and navigate to `http://localhost` to access the shep web interface.
